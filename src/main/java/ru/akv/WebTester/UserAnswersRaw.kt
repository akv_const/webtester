package ru.akv.webtester

data class UserAnswersRaw(val firstName: String, val lastName: String
                        , val answers: Map<String, Answer>) {
    data class Answer(val variantId: String, val answerIds: Set<String>)
}