package ru.akv.webtester

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.InputStream
import javax.xml.bind.JAXBContext

class Tester {
    companion object {
        val logAnswer: Logger = LoggerFactory.getLogger("ru.akv.webtester.TesterAnswers")
    }
    private val testData: TestData

    constructor(pathToDataFile: String) {
        val inputStream = javaClass.classLoader.getResourceAsStream(pathToDataFile)
        this.testData = initTestData(inputStream!!)
    }

    constructor(inputStream: InputStream) {
        this.testData = initTestData(inputStream)
    }

    private fun initTestData(inputStream: InputStream): TestData {
        val jaxbContext = JAXBContext.newInstance(TestData::class.java)
        val jaxbUnmarshaller = jaxbContext.createUnmarshaller()
        return jaxbUnmarshaller.unmarshal(inputStream) as TestData
    }

    fun getTest(): SingleTest = testData.getTest()

    data class CheckedAnswer(val questionId: String, val variantId: String, val answerIds: Set<String>, val isCorrect: Boolean)
    data class TestResult(val firstName: String, val lastName: String, val answers: List<CheckedAnswer>)

    private fun checkRawAnswers(rawAnswers: Map<String, UserAnswersRaw.Answer>): List<CheckedAnswer> {
        val checkedAnswers = mutableListOf<CheckedAnswer>()
        rawAnswers.forEach { (questionId, u) ->
            val isCorrect = testData.questions.find { it.id == questionId }?.variants
                    ?.find { it.id == u.variantId }?.answers
                    ?.mapNotNull { if (it.isCorrect) it.id else null }?.toSet()?.equals(u.answerIds) ?: false
            checkedAnswers.add(CheckedAnswer(questionId, u.variantId, u.answerIds, isCorrect))
        }
        return checkedAnswers
    }
    
    fun processUserAnswers(rawAnswers: UserAnswersRaw) {
        val testResult = TestResult(rawAnswers.firstName, rawAnswers.lastName, checkRawAnswers(rawAnswers.answers))
        outputTestResult(testResult)
    }

    private fun outputTestResult(testResult: TestResult) {
        val sb = StringBuilder()
        sb.appendln("Получен ответ: {")
        sb.appendln("${testResult.lastName} ${testResult.firstName}")
        testResult.answers.forEach {
            val result = if (it.isCorrect) "верно" else "ошибка"
            sb.appendln("Задание ${it.questionId}, вариант ${it.variantId}, ответ ${it.answerIds} ($result)")
        }
        sb.append("}")
        logAnswer.info(sb.toString())
    }
}