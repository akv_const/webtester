@file:Suppress("unused")

package ru.akv.webtester

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonView
import javax.xml.bind.annotation.*
import javax.xml.bind.annotation.adapters.XmlAdapter
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

interface TestView {
    interface Public
}

class TrimStringXmlAdapter : XmlAdapter<String, String>() {
    override fun unmarshal(v: String?): String {
        return v?.trim() ?: ""
    }

    override fun marshal(v: String?): String {
        return v ?: ""
    }
}

class AnswerTypeXmlAdapter : XmlAdapter<String, TestData.Question.Variant.AnswerType>() {
    override fun unmarshal(v: String?): TestData.Question.Variant.AnswerType {
        return TestData.Question.Variant.AnswerType.valueOf(v?.toUpperCase() ?: "")
    }

    override fun marshal(v: TestData.Question.Variant.AnswerType?): String {
        return v?.name?.toLowerCase() ?: ""
    }
}

@XmlRootElement(name = "test")
@XmlAccessorType(XmlAccessType.NONE)
data class TestData(
        @field:XmlElement(name = "theme")
        @field:XmlJavaTypeAdapter(value = TrimStringXmlAdapter::class)
        val theme: String,
        @field:XmlElement(name = "question")
        val questions: List<Question>) {
    constructor() : this("", mutableListOf())

    @XmlRootElement(name = "question")
    @XmlAccessorType(XmlAccessType.NONE)
    data class Question(
            @field:XmlAttribute(name = "id")
            val id: String,
            @field:XmlElement(name = "variant") val variants: List<Variant>) {
        constructor() : this("", mutableListOf())

        @XmlRootElement(name = "variant")
        @XmlAccessorType(XmlAccessType.NONE)
        @JsonView(TestView.Public::class)
        data class Variant(
                @field:XmlAttribute(name = "id")
                val id: String,
                @field:XmlElement(name = "title")
                @field:XmlJavaTypeAdapter(value = TrimStringXmlAdapter::class)
                val title: String,
                @field:XmlElement(name = "text")
                @field:XmlJavaTypeAdapter(value = TrimStringXmlAdapter::class)
                val text: String,
                @field:XmlAttribute(name = "type")
                @field:XmlJavaTypeAdapter(value = AnswerTypeXmlAdapter::class)
                val answerType: AnswerType,
                @field:XmlElement(name = "answer")
                val answers: List<Answer>) {
            constructor() : this("", "", "", AnswerType.SINGLE, mutableListOf())

            enum class AnswerType { SINGLE, MULTI, TEXT }

            @XmlRootElement(name = "answer")
            @XmlAccessorType(XmlAccessType.NONE)
            class Answer(
                    @field:XmlAttribute(name = "id")
                    @JsonView(TestView.Public::class)
                    val id: String,
                    @field:XmlValue
                    @field:XmlJavaTypeAdapter(value = TrimStringXmlAdapter::class)
                    @JsonView(TestView.Public::class)
                    val text: String,
                    @field:XmlAttribute(name = "isCorrect")
                    val isCorrect: Boolean) {
                constructor() : this("", "", false)

                override fun toString(): String {
                    return "Answer(id=$id, text=$text, isCorrect=$isCorrect)"
                }
            }
        }
    }

    fun getTest(): SingleTest {
        val res = mutableMapOf<String, Question.Variant>()
        for (it in questions) {
            res[it.id] = it.variants.random()
        }
        return SingleTest(theme, res)
    }
}

@JsonView(TestView.Public::class)
data class SingleTest(val theme: String, val questions: Map<String, TestData.Question.Variant>) {
    @JsonIgnore
    val description: String

    init {
        val sb = StringBuilder("Тема: $theme [")
        questions.forEach {
            sb.append("\n\tвопрос ${it.key}, вариант ${it.value.id}")
        }
        sb.append(']')
        description = sb.toString()
    }
}