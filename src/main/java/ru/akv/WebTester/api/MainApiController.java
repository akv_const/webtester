package ru.akv.webtester.api;

import com.fasterxml.jackson.annotation.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import ru.akv.webtester.SingleTest;
import ru.akv.webtester.TestView;
import ru.akv.webtester.Tester;
import ru.akv.webtester.UserAnswersRaw;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

@RestController
public class MainApiController
{
	static final Logger log = LoggerFactory.getLogger(MainApiController.class);
	@Inject
	Tester tester;

	@GetMapping("api/test")
	@JsonView(TestView.Public.class)
	public SingleTest getTest(HttpServletResponse response)
	{
		SingleTest test = tester.getTest();
		log.debug("Запрошен тест.\n" + test.getDescription());
		return test;
	}

	@PostMapping("api/answers")
	public void postAnswers(@RequestBody UserAnswersRaw res)
	{
		tester.processUserAnswers(res);
	}
}
