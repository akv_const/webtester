package ru.akv.webtester

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import javax.servlet.ServletContext

@Configuration
@EnableWebMvc
@ComponentScan("ru.akv.WebTester")
open class WebConfig(servletContext: ServletContext): WebMvcConfigurer {
    private val testDataPath: String

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        super.addResourceHandlers(registry)
        registry.addResourceHandler("/static/**")
                .addResourceLocations("/WEB-INF/static/")
    }

    init {
        testDataPath = servletContext.getInitParameter("testDataPath") ?: ""
    }

    @Bean
    open fun tester(): Tester {
        return if (testDataPath == "")
            Tester(javaClass.classLoader.getResourceAsStream("test.xml")!!)
        else
            Tester(testDataPath)
    }
}