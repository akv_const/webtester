package ru.akv;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.kotlin.KotlinModule;
import org.junit.Assert;
import org.junit.Test;
import ru.akv.webtester.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.Objects;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void getTestData() throws JAXBException
    {
        InputStream is = getClass().getClassLoader().getResourceAsStream("test.xml");
        Assert.assertNotNull(is);
        JAXBContext jaxbContext = JAXBContext.newInstance(TestData.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        TestData testData = (TestData)jaxbUnmarshaller.unmarshal(is);
    }

    @Test
    public void checkTestDescription()
    {
        Tester tester = new Tester(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("test.xml")));
        SingleTest test = tester.getTest();
        System.out.println(test.getDescription());
    }

    @Test
    public void checkTest2Json()
    {
        try
        {
            Tester tester = new Tester(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("test.xml")));
            SingleTest test = tester.getTest();
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper
                    .registerModule(new KotlinModule())
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .disable(MapperFeature.DEFAULT_VIEW_INCLUSION)
                    .writerWithView(TestView.Public.class)
                    .writeValueAsString(test);
            System.out.println(json);
        }
        catch (Exception ex)
        {
            System.err.println(ex.toString());
        }
    }
}
